## FIRMWARE DUMP
### sys_tssi_64_armv82_infinix-user 14 UP1A.231005.007 768494 release-keys
- Transsion Name: Infinix NOTE 30 Pro
- TranOS Build: X678B-H894OPQ-U-OP-241210V820
- TranOS Version: xos14.0.0
- Brand: INFINIX
- Model: Infinix-X678B
- Platform: mt6789 (Helio G99)
- Android Build: UP1A.231005.007
- Android Version: 14
- Kernel Version: 5.10.209
- Security Patch: 2024-12-05
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Screen Density: 480
- Fingerprint: Infinix/X678B-OP/Infinix-X678B:14/UP1A.231005.007/241210V820:user/release-keys
